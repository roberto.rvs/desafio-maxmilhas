infra-create:
	 	 	docker-compose -f terraform-compose.yaml up
apps-create:
			docker-compose -f apps-compose.yaml up
remove-all:
			docker-compose -f apps-compose.yaml rm -s -f
			docker-compose -f terraform-compose.yaml run terraform terraform destroy --auto-approve
			docker-compose -f terraform-compose.yaml rm -s -f
