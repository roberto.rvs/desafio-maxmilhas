# Node.js SQS API

## Descrição
Neste repositório você encontrará uma [API Rest](https://gitlab.com/roberto.rvs/desafio-maxmilhas/-/tree/main/api) escrita em [Node.js](https://nodejs.org/en/)  responsável por enviar mensagens de sugestão para uma fila [SQS](https://aws.amazon.com/sqs/) e uma aplicação [Worker]() responsável por consumir estas mensagens e armazená-las em uma base de dados [MongoDB](https://www.mongodb.com/what-is-mongodb).

## Pré-requisitos
Para a execução deste projeto você deverá possuir uma conta na ``AWS`` e um usuário com as devidas permissões para acessar os recursos ``S3`` e ``SQS``. 
>Informações de como criar uma conta free tier [aqui](https://aws.amazon.com/free). Para informações de criação de policies de acesso ao S3 e SQS clique [aqui](https://docs.aws.amazon.com/AmazonS3/latest/userguide/access-policy-language-overview.html) e [aqui](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-basic-examples-of-sqs-policies.html).

Você precisará também de um ambiente Linux com os pacotes ``docker`` e ``docker-compose`` devidamente instalados e atualizados.
> Informações de como realizar a instalação do docker [aqui](https://docs.docker.com/engine/install/). Para informações de como instalar o docker-compose clique [aqui](https://docs.docker.com/compose/install/).
## Node.js
A tecnologia escolhida para a criação das aplicações foi o ``Node.js`` por já possuir familiaridade com a linguagem ``javascript``, e por ela dispor de uma grande quantidade de módulos que facilitam o desenvolvimento de APIs.

## Aplicações

### [``Terraform``](https://gitlab.com/roberto.rvs/desafio-maxmilhas/-/tree/main/terraform)
Ferramenta utilizada para o provisionamento da fila SQS.

### ``MongoDB``
Banco de dados resposável por armazenar as sugestões recebidas. 

### [``API``](https://gitlab.com/roberto.rvs/desafio-maxmilhas/-/tree/main/api) 
Responsável pelo envio de mensagens à fila SQS. 

#### Fluxogramas
```mermaid
graph TB
  subgraph "API"
  Node1((Início)) --> Node2[POST Request]
  Node2 --> Node3{If}
  Node3 -->|Err| Node4[Error + Err]
  Node4 -->Node7((Fim))
  Node3 -->|Else| Node5[Envia para fila SQS]
  Node5 --> Node6["Success" + MessageId]
  Node6 --> Node8((Fim))
  style Node1 fill:#f9f
  style Node7 fill:#bbf
  style Node8 fill:#bbf
end
```
```mermaid
graph TB
  subgraph "API"
  Node1((Início)) --> Node2[GET Request]
  Node2 --> Node3[(Conecta no MongoDB)]
  Node3 --> Node4[Mostra sugestões salvas]
  Node4 --> Node5((Fim))
  style Node1 fill:#f9f
  style Node5 fill:#bbf
end
```

Suas funcionalidades são:

- _**POST**_ : Envia mensagens através do endpoint ``` http://localhost:5000/api/suggestions ``` .

- _**GET**_ : Exibe as mensagens armazenadas no MongoDB também através do endpoint ```http://localhost:5000/api/suggestions```.

#### [``Worker``](https://gitlab.com/roberto.rvs/desafio-maxmilhas/-/tree/main/worker) 
Responsável por receber as mensagens da fila do SQS a cada 100 milisegundos, salvá-las no MongoDB e depois removê-las da fila.

#### Fluxograma
```mermaid
graph TB
  subgraph "Worker"
  Node1((Início)) -->Node2[Recebe mensagem]
  Node2 --> Node3[(Salva no MongoDB)]
  Node2-->|setInterval 100 ms|Node2
  Node3 --> Node4{sqs.deleteMessage}
  Node4 -->|Err| Node5[Delete error + Err]
  Node5 --> Node8((Fim))
  Node4 -->|else| Node6[Message Deleted + data]
  Node6 --> Node7((Fim))
  style Node1 fill:#f9f
  style Node7 fill:#bbf
  style Node8 fill:#bbf
end
```

## Execução

- **1° Passo**: Para realizar a execução, você deverá primeiramente substituir as variáveis ``ARG_AWS_ACCESS_KEY_ID``, ``ARG_AWS_SECRET_ACCESS_KEY`` que estão presentes no arquivo [terraform-compose.yaml](https://gitlab.com/roberto.rvs/desafio-maxmilhas/-/blob/main/terraform-compose.yaml) e as variáveis ``AWS_ACCESS_KEY_ID`` e ``AWS_SECRET_ACCESS_KEY`` que estão presentes no arquivo [apps-compose.yaml](https://gitlab.com/roberto.rvs/desafio-maxmilhas/-/blob/main/apps-compose.yaml) com o ``Access Key ID`` e ``Secret Access Key`` gerados na AWS no momento da criação do usuário.
- **2° Passo** : Criar na AWS um S3 bucket com o nome ``sqs-bucket``. Este bucket será encarregado de armazenar o arquivo de estado (**terraform.tfstate**) do terraform.
> Caso queira utilizar um bucket já existente, mudar o nome do bucket no arquivo [terraform/backend.tf](https://gitlab.com/roberto.rvs/desafio-maxmilhas/-/blob/main/terraform/backend.tf).
- **3° Passo** : Tendo os passos acima resolvidos, podemos começar o provisionamento do SQS. \
Para executar o terraform rode o comando:
```shell
make infra-create
```
Ele será encarregado de executar o arquivo ``terraform-compose.yaml`` que builda uma nova imagem terraform com as variáveis de ambiente e arquivos ``.tf`` necessários para o provisionamento da infraestrutura.
>``Atenção``: Caso você queira utilizar uma chave kms já criada em sua conta AWS, mude ``kms_master_key_id`` para o seu id e comente as linhas referentes ao recurso ``aws_kms_key``. Caso contrário, será gerada uma nova chave kms com uma janela de deleção de 7 dias quando a exclusão for selecionada.

Abaixo segue o ``IAM Policy`` com as ações necessárias para a API e Worker:
```shell
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "sqs:DeleteMessage",
                "sqs:SendMessage",
                "ReceiveMessage"
            ],
            "Resource": [
                "arn:aws:sqs:*:*:${var.aws_sqs_name}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": "sqs:ListQueues",
            "Resource": "*"
        }
    ]
}
```
> o ``IAM Policy`` e outras configurações podem ser verificadas no arquivo [sqs.tf](https://gitlab.com/roberto.rvs/desafio-maxmilhas/-/blob/main/terraform/sqs.tf).

 Ao final da execução será printado na tela o output ``queue_id`` como visto em seguida:

```shell
terraform_1  | Apply complete! Resources: 1 added, 0 changed, 0 destroyed.
terraform_1  | 
terraform_1  | Outputs:
terraform_1  | 
terraform_1  | queue_id = "https://sqs.us-east-2.amazonaws.com/XXXXXXXXXXXX/queue_test"
```
O valor do ``queue_id`` será importante para que as aplicações conectem na fila, podendo assim enviar, receber e deletar mensagens.

- **4° Passo**: Com o SQS provisionado, será necessário atribuir o valor do output ``queue_id`` na variável ``AWS_QUEUE_URL`` presente no arquivo ``apps-compose.yaml`` tanto para o service [api](https://gitlab.com/roberto.rvs/desafio-maxmilhas/-/blob/main/apps-compose.yaml#L22) quanto para o [worker](https://gitlab.com/roberto.rvs/desafio-maxmilhas/-/blob/main/apps-compose.yaml#L36).

- **5° Passo**: Tendo todos os passos anteriores finalizados, devemos deployar as aplicações Worker e API junto com o MongoDB. Para isto rode o comando:
```shell
make apps-create
```
Feito isto, temos a infraestrutura devidamente provisionada e todas as aplicações em funcionamento.

## Validação
Para validação, faremos requisições _GET_ e _POST_ através do ``curl``. Caso você se sinta mais confortável, poderá utilizar o [Postman](https://www.postman.com/).

## API Rest
- _POST_ : Nesta validação enviaremos uma sugestão de melhorias que será enfileirada no SQS que provisionamos anteriormente:

```shell
curl http://localhost:5000/api/suggestions \
    -X POST \
    -H "Content-Type: application/json" \
    -d '{"message":"Sugestão de melhorias", "user":"Roberto Rivelino"}'
```
Resultado: 
```shell
api           | Success 107612d1-009f-46d5-abac-549a9d827208
```

- _GET_ : Através do _GET_ conseguimos ver as mensagens armazenadas no MongoDB:

```shell
curl http://localhost:5000/api/suggestions \
    -X GET \
    -H "Content-Type: application/json"
```

Resultado:
```shell
[{"_id":"60b5227ceca9800012972289","message":"Sugestão de melhorias","user":"Roberto Rivelino","createdAt":"2021-05-31T17:53:00.466Z","__v":0}]
```

## Worker
No Worker conseguimos acompanhar o recebimento das mensagens que estão na fila SQS, o armazenamento no MongoDB e por fim, a remoção das mensagens:

```shell
worker        | {
worker        |   message: {
worker        |     Message: {
worker        |       StringValue: 'Sugestão de melhorias',
worker        |       StringListValues: [],
worker        |       BinaryListValues: [],
worker        |       DataType: 'String'
worker        |     },
worker        |     User: {
worker        |       StringValue: 'Roberto Rivelino',
worker        |       StringListValues: [],
worker        |       BinaryListValues: [],
worker        |       DataType: 'String'
worker        |     }
worker        |   }
worker        | }
worker        | Message Deleted {
worker        |   ResponseMetadata: { RequestId: 'a6d2e495-e870-5ed4-a30e-bdbaccced00e' }
worker        | }
```

## Remoção
Para remover todos os containers criados e o ``destroy`` da infraestrutura basta executar o comando:
```shell
make remove-all
```

## Trabalhos Futuros
Como próximo passo, pretendo buscar uma solução para automatizar a passagem da configuração do endpoint do SQS  ``queue_id`` entre o provisionamento da infraestrutura e as aplicações API e Worker, facilitando a comunicação com a fila sem precisar de abordagens manuais. \
Foi realizado alguns testes com a ferramenta [HashiCorp Consul](https://www.consul.io/) ao longo do desenvolvimento. Entretanto, a carência de conhecimento nesta ferramenta inviabilizou seu uso.

