// Load the AWS SDK for Node.js
var AWS = require("aws-sdk");
const Suggestion = require("./models/Suggestion");
// Set the region
AWS.config.update({ region: process.env.AWS_REGION });

// Create an SQS service object
var sqs = new AWS.SQS({ apiVersion: "2012-11-05" });

var params = {
  AttributeNames: ["User", "Message"],
  MaxNumberOfMessages: 10,
  MessageAttributeNames: ["All"],
  QueueUrl: process.env.AWS_QUEUE_URL,
  VisibilityTimeout: 20,
  WaitTimeSeconds: 0,
};

const handleMessage = async (message) => {
  return new Promise(async (resolve, reject) => {
    console.log({ message: message.MessageAttributes });
    const suggestion = new Suggestion({
      message: message.MessageAttributes.Message.StringValue,
      user: message.MessageAttributes.User.StringValue,
    });
    await suggestion.save();

    let deleteParams = {
      QueueUrl: process.env.AWS_QUEUE_URL,
      ReceiptHandle: message.ReceiptHandle,
    };
    sqs.deleteMessage(deleteParams, function (err, data) {
      if (err) {
        console.log("Delete Error", err);
        reject(err);
      } else {
        console.log("Message Deleted", data);
        resolve(data);
      }
    });
  });
};

const receiveMessage = () => {
  return new Promise((resolve, reject) => {
    sqs.receiveMessage(params, async function (err, data) {
      if (err) {
        console.log("Receive Error", err);
        reject(err);
      } else if (data.Messages) {
        for (let message of data.Messages) {
          await handleMessage(message);
        }
        resolve(true);
      }
    });
  });
};
module.exports = { receiveMessage };
