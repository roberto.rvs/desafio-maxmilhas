const mongoose = require("mongoose");

const schema = mongoose.Schema({
  message: String,
  user: String,
  createdAt: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Suggestion", schema);
