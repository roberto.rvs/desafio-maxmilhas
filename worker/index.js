require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const { receiveMessage } = require("./sqs");

mongoose
  .connect(process.env.MONGODB_URL, { useNewUrlParser: true })
  .then(() => {
    const app = express();
    app.listen(5001, () => {
      console.log("Server has started!");
      setInterval(async () => {
        receiveMessage();
      }, 100);
    });
  });
