resource "aws_kms_key" "this" {
  deletion_window_in_days = 7
}

resource "aws_sqs_queue" "queue_maxmilhas" {
  name                              = var.aws_sqs_name
  delay_seconds                     = 0
  max_message_size                  = 2048
  message_retention_seconds         = 86400
  receive_wait_time_seconds         = 10
  fifo_queue                        = false
  kms_data_key_reuse_period_seconds = 300
  kms_master_key_id                 = aws_kms_key.this.id
  policy                            = <<EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "sqs:DeleteMessage",
                "sqs:SendMessage",
                "ReceiveMessage"
            ],
            "Resource": [
                "arn:aws:sqs:*:*:${var.aws_sqs_name}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": "sqs:ListQueues",
            "Resource": "*"
        }
    ]
}
EOF

  tags = {
    Environment = "Dev"
    Provider    = "Terraform"
  }
}