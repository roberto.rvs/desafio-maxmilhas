terraform {
  backend "s3" {
    bucket = "sqs-bucket"
    key    = "sqs/terraform.tfstate"
    region = "us-east-2"
  }
}