const express = require("express");
const router = express.Router();
const Suggestion = require("./models/Suggestion.js");
const sqs = require("./sqs.js");

router.get("/suggestions", async (req, res) => {
  const suggestions = await Suggestion.find();
  res.send(suggestions);
});

router.post("/suggestions", async (req, res) => {
  const messageId = await sqs.sendMessage(req.body.message, req.body.user);
  res.send(messageId);
});

module.exports = router;
