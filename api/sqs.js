// Load the AWS SDK for Node.js
var AWS = require("aws-sdk");
// Set the region
AWS.config.update({ region: process.env.AWS_REGION });

// Create an SQS service object
var sqs = new AWS.SQS({ apiVersion: "2012-11-05" });

const sendMessage = (message, user) => {
  return new Promise((resolve, reject) => {
    var params = {
      // Remove DelaySeconds parameter and value for FIFO queues
      DelaySeconds: 10,
      MessageAttributes: {
        Message: {
          DataType: "String",
          StringValue: message,
        },
        User: {
          DataType: "String",
          StringValue: user,
        },
      },
      MessageBody: "Sugestão de melhoria das ferramentas",
      QueueUrl: process.env.AWS_QUEUE_URL,
    };
    sqs.sendMessage(params, function (err, data) {
      if (err) {
        console.log("Error", err);
        reject(err);
      } else {
        console.log("Success", data.MessageId);
        resolve(data.MessageId);
      }
    });
  });
};

module.exports = { sendMessage };
