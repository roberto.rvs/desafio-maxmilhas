const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();
const routes = require("./routes");

mongoose
  .connect(process.env.MONGODB_URL, { useNewUrlParser: true })
  .then(() => {
    const app = express();
    app.use(express.json()); // new
    app.use("/api", routes);

    app.listen(5000, () => {
      console.log("Server has started!");
    });
  });
